# COMP3109 Collaborative Test Suite

## Introduction

Test suite curated by the students of Sydney University's COMP3109 2015 course.

## Getting the Test Suite

### The repository _doesn't_ have your test suite

1. Open a Terminal
2. Navigate to your team's assignment *Git* repository
3. Delete your current `test` folder: `rm -r test`
4. Commit your changes (of deleting the `test` folder): `git commit -a "Deleting old tests"`
5. Type: `git submodule add https://bitbucket.org/Mayzie/comp3109_assignment2_tests.git test`
6. Push your changes: `git push`
7. Voila!

### The repository _does_ have your test suite

1. Open a Terminal
2. Navigate to your team's assignment *Git* repository
3. (If you haven't already) Pull the latest changes
4. Type: `git submodule update --init --recursive`
5. Voila!

## Updating the Test Suite

1. Open a Terminal
2. Navigate to your team's assignment *Git* repository
3. Type: `git submodule update --remote test`
4. Voila!

## Submitting Additional Tests

You're looking at this if you would like to contribute to the test suite to 
benefit others, yay!

You can do this by either:

1. Submitting a Pull Request
2. Creating an Issue with:
	1. The name of your test (file name, e.g. `factorial.imp`)
	2. The contents of your test file

## Additional Resources

* Git Submodule tutorial: https://git-scm.com/book/en/v2/Git-Tools-Submodules
* Making BitBucket pull requests: https://www.atlassian.com/git/tutorials/making-a-pull-request

*Daniel Collis*
